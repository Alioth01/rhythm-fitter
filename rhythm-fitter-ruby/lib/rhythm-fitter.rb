# frozen_string_literal: true

require_relative File.join('rhythm-fitter', 'version')

module RhythmFitter

  class Error < StandardError; end

  %w(
    datum.rb
    measurement.rb
    voice.rb
    manager.rb
  ).each { |f| require_relative File.join('rhythm-fitter', f) }

end
