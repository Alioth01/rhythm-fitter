module RhythmFitter

	class Voice
	  attr_reader :name, :data
	
	  def initialize(key, data = [])
	    @name = key
	    @data = data.map { |d| Datum.new(d) }
	    @scan_end = self.data[-1].ref + 10 # allow 10-second lee-way
	    nop = 0
	  end
	
	  def scan
	    metros = (30..144).map { |n| n }
	    subdivs = [(1..11).map { |n| n }, (4...8).map { |n| n**2 }].flatten
	    self.data.each do
	      |d|
		    metros.each do
		      |m|
		      mperiod = 60.0/m.to_f
		      subdivs.each do
		        |sdiv|
	          now = 0
	          num = 1
	          while now < @scan_end*4
	            meas = Measurement.new(num, sdiv, m)
	            cur_rms = d.measure(meas) 
	STDERR.puts("ref: %.4f scanning(%d/%d @ %.2f = %.4f): %.4f winner: %.4f\n" % [ d.ref, num, sdiv, m, meas.to_f,  cur_rms, d.best_measure.rms ])
	            num += 1 
	            now += meas.to_f
	          end
		      end
		    end
	    end
	  end
	
	  def to_s
	    result = self.name + ":\n"
	    self.data.each { |d| result += d.to_s }
	    result
	  end
	
	end

end
