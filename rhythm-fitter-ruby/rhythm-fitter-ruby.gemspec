# frozen_string_literal: true

require_relative "lib/rhythm-fitter/version"

Gem::Specification.new do |spec|
  spec.name = "rhythm-fitter-ruby"
  spec.version = RhythmFitter::VERSION
  spec.authors = ["Nicola Bernardini", "Pasquale Citera"]
  spec.email = ["nicola.bernardini.rome@gmail.com"]

  spec.summary = "rhythm-fitter fits any set of time data to musical rhythm grids."
  spec.description = "rhythm-fitter is a tentative library to fit any set of time data to musical rhythm grids of various sorts."
  spec.homepage = "https://rubygems.org/gems/rhythm-fitter-ruby"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org/gems/rhythm-fitter-ruby"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/SMERM/rhythm-fitter"
  spec.metadata["changelog_uri"] = "https://gitlab.com/SMERM/rhythm-fitter/rhythm-fitter-ruby/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
